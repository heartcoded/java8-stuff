package optional;

import java.util.Optional;

public class OptionalMain {

    public static void main(String[] args){
        String[] str = new String[5];
        str[2] = "Some text here.";

        Optional<String> empty = Optional.empty();
        System.out.println("empty");

        Optional<String> value = Optional.of(str[2]);
        System.out.println(value);
        if (value.isPresent()) {
            System.out.println(value.get());
        }

        // in case we expect some null values we can use the ofNullable API; if its null
        // it returns an empty Optional object
        Optional<String> maybeValue = Optional.ofNullable(str[4]);
        System.out.println(maybeValue);
        Optional<String> maybeValue1 = Optional.ofNullable(str[2]);
        System.out.println(maybeValue1);

        // ifPresent(..) enables us to run some code on the wrapped value if it's found
        // to be non-null.
        Optional<String> myName = Optional.of("Diana Brad");
        myName.ifPresent(n -> System.out.println(n.length()));

        // Default value with orElse
        String nullName = null;
        String name = Optional.ofNullable(nullName).orElse("Default name");
        System.out.println("Name: " + name);

        // Same stuff with orElseGet but using a functional interface
        String nullName2 = null;
        String name2 = Optional.ofNullable(nullName2).orElseGet(() -> "Default name 2");
        System.out.println("Name2: " + name2);

        // filter
        Integer year = 2019;
        Optional<Integer> optionalYear = Optional.of(year);
        boolean is2019 = optionalYear.filter(y -> y == 2019).isPresent();
        boolean is2018 = optionalYear.filter(y -> y == 2018).isPresent(); // returns an empty Optional
        System.out.println("Is 2019? " + is2019);
        System.out.println("Is 2019? " + is2018);

        Book book1 = new Book("The picture of Dorian Gray", 45D);
        Book book2 = null;
        boolean result = Optional.ofNullable(book1).map(Book::getPrice).filter(p -> p >= 10).filter(p -> p <= 50)
                .isPresent();
        boolean result2 = Optional.ofNullable(book2).map(Book::getPrice).filter(p -> p >= 10).filter(p -> p <= 50)
                .isPresent();
        System.out.println("Result: "+result);
        System.out.println("Result2: "+result2);
    }
}
