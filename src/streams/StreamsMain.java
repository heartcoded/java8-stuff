package streams;

import java.util.Arrays;
import java.util.IntSummaryStatistics;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class StreamsMain {

    /**
     * testing different stuff with Java 8 Streams
     */
    public static void main(String[] args) {
        List<Student> students = Arrays.asList(
                new Student("Ella",18),
                new Student("Diana",20),
                new Student("Jessica",21),
                new Student("Patricia",17),
                new Student("Tom",24),
                new Student("Isa",21),
                new Student("Collen",17),
                new Student("Eric",22)
        );

        //collect only students which name starts with 'E'
        List<Student> filteredStudents = students
                .stream()
                .filter(s->s.getName().startsWith("E"))
                .collect(Collectors.toList());
        System.out.println(filteredStudents);

        //map by age
        System.out.println("--------------------------------------");
        Map<Integer, List<Student>> studentsByAge = students
                .stream()
                .collect(Collectors.groupingBy(s-> s.getAge()));
        studentsByAge.forEach((age,s) -> System.out.format("age %s: %s\n ",age,s));


        //determine the average of age of all students
        System.out.println("--------------------------------------");
        Double averageAge = students
                .stream()
                .collect(Collectors.averagingInt(s->s.getAge()));
        System.out.println("Average age: "+averageAge);

        //stats
        System.out.println("--------------------------------------");
        IntSummaryStatistics ageSummary =
                students
                        .stream()
                        .collect(Collectors.summarizingInt(s->s.getAge()));
        System.out.println("Age summary: "+ageSummary);

        System.out.println("--------------------------------------");
        Map<Integer, String> map  = students
                .stream()
                .collect(Collectors.toMap(
                        s-> s.getAge(),
                        s-> s.getName(),
                        (name1, name2) -> name1+";"+name2));
        System.out.println(map);

        System.out.println("--------------------------------------");
        Stream.of("first element","second element","third element")
                .findFirst()
                .ifPresent(System.out::println);

        System.out.println("--------------------------------------");
        List<String> randomList = Arrays.asList("a1","b2","c2","b1","b3","d1");
        randomList
                .stream()
                .filter(s -> s.startsWith("b"))
                .map(String::toUpperCase)
                .sorted()
                .forEach(System.out::println);

        System.out.println("--------------------------------------");
        Arrays.stream(new int[] {1,2,3})
                .map(n->2*n + 1)
                .average()
                .ifPresent(System.out::println);
    }


}
